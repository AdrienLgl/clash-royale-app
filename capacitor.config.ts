import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'clash-jr',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
