// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: any = {
  production: false,
  // eslint-disable-next-line max-len
  token: `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImQxYmE2NTU1LTVmNDgtNGFhOS05ODJmLTFmMjc0MThhM2RiMiIsImlhdCI6MTY0NDMyMjA2MCwic3ViIjoiZGV2ZWxvcGVyL2RlZTY5Y2MwLWNiNjAtNWQ5Zi02ZTY0LTZiZmZlZWI1ZjkwOSIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNzYuMTkwLjI1MC44IiwiMTc2LjE0My43Ny4yOSIsIjQ1Ljc5LjIxOC43OSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.Samch59iw4xqjYWH1r6h1g4oDAhTO7IgxWO5Bg5wX1vbDBx6mnPiUpvOUhk5vrTgozlmkE9qkp0QHkjRpa-MQw`
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
