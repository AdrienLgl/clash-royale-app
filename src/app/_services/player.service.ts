/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


const XSSI_PREFIX = /^\)\]\}',?\n/;

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  async findPlayer(id: string) {
    id = id.replace('#', '');
    const req: RequestInit = this.getRequest();
    const response = await fetch(`https://proxy.royaleapi.dev/v1/players/%23${id}`, req);
    const data = response.json();
    return data;
  }

  async getBattlelog(id: string) {
    id = id.replace('#', '');
    const req: RequestInit = this.getRequest();
    const response = await fetch(`https://proxy.royaleapi.dev/v1/players/%23${id}/battlelog`, req);
    const data = response.json();
    return data;
  }


  async getClan(id: string) {
    id = id.replace('#', '');
    const req: RequestInit = this.getRequest();
    const response = await fetch(`https://proxy.royaleapi.dev/v1/clans/%23LRR290VJ`, req);
    const data = response.json();
    return data;
  }

  async getBadge(id: string) {
    const req: RequestInit = this.getRequest();
    const response = await fetch(`https://royaleapi.github.io/cr-api-data/json/alliance_badges.json`, req);
    const data = response.json();
    return data;
  }

  findBadge(id: string): Promise<any> {
    return new Promise(resolve => {
      this.readJson('badge').subscribe((response) => {
        Object.values(response).forEach((badge) => {
          if (badge.id === id) {
            resolve(badge);
          }
        });

        resolve(null);
      });
    });
  }

  findArena(id: string): Promise<any> {
    return new Promise(resolve => {
      this.readJson('arena').subscribe((response) => {
        Object.values(response).forEach((arena) => {
          if (arena.id === id) {
            resolve(arena);
          }
        });

        resolve(null);
      });
    });
  }

  readJson(url: string) {
    return this.http.get(`assets/_data/${url}.json`);
  }

  getRequest(): RequestInit {
    const myHeaders = new Headers();
    myHeaders.append('Accept', 'application/json, text/plain, */*');
    myHeaders.append('Authorization', `Bearer ${environment.token}`);
    return {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
  }

}
