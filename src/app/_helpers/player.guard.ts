import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerGuard implements CanActivate {

  private _storage: Storage | null = null;

  constructor(private storage: Storage, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.checkPlayer().then((statut) => {
        if (statut) {
          return true;
        } else {
          this.router.navigate(['/home']);
          return false;
        }
      });
    return true;
  }

  checkPlayer(): Promise<boolean> {
    return new Promise(async (resolve) => {
      this._storage = await this.storage.create();
      const player = await this._storage.get('player');
      console.log(player);
      if (player === null) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  }
  
}
