import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoaderService } from '../_services/loader.service';
import { PlayerService } from '../_services/player.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  players: string[] = [];
  player: any;
  arena: any;
  badge: any;
  private _storage: Storage | null = null;

  constructor(private playerService: PlayerService, private loaderService: LoaderService, private storage: Storage, private router: Router, public alertController: AlertController) {
    console.log('constructor');
  }

  async ngOnInit() {
    this._storage = await this.storage.create();
    const playerId = await this._storage.get('player');
    this.players = playerId !== undefined || playerId !== null ? [playerId] : [];
    this.getPlayers();
  }

  async ionViewDidEnter() {
    this._storage = await this.storage.create();
    const playerId = await this._storage.get('player');
    this.players = playerId !== undefined || playerId !== null ? [playerId] : [];
    this.getPlayers();
  }

  async ionViewDidLoad() {
    console.log('load');
  }

  async ionViewWillEnter() {
    this._storage = await this.storage.create();
    const playerId = await this._storage.get('player');
    this.players = playerId !== undefined || playerId !== null ? [playerId] : [];
    this.getPlayers();
  }


  getPlayers(): void {
    console.log('loading');
    this.loaderService.show();
    this.players.forEach((player) => {
      console.log(player);
      this.playerService.findPlayer(player).then((response) => {
        this.player = response;
        this.playerService.findArena(response.arena.id).then((arena) => {
          this.arena = arena;
        });

        if (response.clan) {
          this.playerService.findBadge(response.clan.badgeId).then((badge) => {
            this.badge = badge;
          });
        } else {
          this.badge = {name: 'no_clan'};
          this.player['clan'] = {
            name: 'Pas de clan'
          };
        }
        console.log(this.player.name);
      });
    });

    this.loaderService.hide();
  }

  exit(): void {
    this._storage.clear();
    this.redirectTo('/home');
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Déconnexion',
      message: 'Vous allez changer de compte ? Etes-vous sûr ?',
      buttons: [
        {
          text: 'Annuler',
          handler: () => {
            
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.exit();
          }
        }
      ]
    });
    await alert.present();
    const { role } = await alert.onDidDismiss();
  }

  redirectTo(uri:string){
    this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
    this.router.navigate([uri]));
 }

}
