import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoaderService } from '../_services/loader.service';
import { PlayerService } from '../_services/player.service';

export interface Player {
  id: string;
  name: string;
  clan: string;
  badge: string;
  trophy: number;
  score: number;
}

export interface Battle {
  date: string;
  player1: Player;
  player2: Player; 
}

export interface Battles {
  [key: string]: Battle;
}

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnDestroy{

  matchs: any;
  battles: any[];
  private _storage: Storage | null = null;

  constructor(private storage: Storage, private playerService: PlayerService, public alertController: AlertController, private loaderService: LoaderService) {}

  async ionViewWillEnter() {
    const storage = await this.storage.create();
    this._storage = storage;
    this.loadMatchs();
  }

  ngOnDestroy(): void {
    this._storage.set('matchs', JSON.stringify(this.matchs));
  }

  async loadMatchs() {
    const matchs = await this._storage.get('matchs');
    this.matchs = matchs === undefined || matchs === null ? {} : JSON.parse(matchs);
    this.getBattle();
    this.getPlayersInformations();
  }


  getPlayersInformations(): void {
    if (this.matchs) {
      Object.values(this.matchs).forEach((match: any) => {
        this.setMatch(match.player1.id, match.player2.id);
      });
    }
  }

  getBattle() {
    if (this.matchs) {
      this.battles = Object.values(this.matchs);
      this.loaderService.hide();
    }
  }

  findPlayers(player1: string, player2: string): boolean {
    return this.matchs !== null && this.matchs !== undefined && this.matchs[`${player1}/${player2}`] !== undefined;
  }

  checkPlayer(player1: any, player2: any): void {
    this.playerService.findPlayer(player1).then((response) => {
      this.playerService.findPlayer(player2).then((res) => {
        this.setMatch(player1, player2);
      }, (err) => {
        this.presentAlert();
      });
    }, (error) => {
      this.presentAlert();
    })
  }

  setMatch(player1: string, player2: string) {
    setTimeout(() => {
      this.loaderService.show();
    }, 10);
    this.playerService.getBattlelog(player1).then((battlelog: any[]) => {
      battlelog.forEach((match) => {
        if (match.opponent[0].tag === player2) {
          if (!this.findPlayers(player1, player2)) {
            const battle: Battle = {
              player1: {
                badge: match.team[0].clan ? match.team[0].clan.badgeId : 'no_clan',
                clan: match.team[0].clan ? match.team[0].clan.name : 'No clan',
                id: match.team[0].tag,
                name: match.team[0].name,
                score: match.team[0].crowns > match.opponent[0].crowns ? 1 : 0,
                trophy: match.team[0].startingTrophies
              },
              player2: {
                badge: match.opponent[0].clan ? match.opponent[0].clan.badgeId : 'no_clan',
                clan: match.opponent[0].clan ? match.opponent[0].clan.name : 'No clan',
                id: match.opponent[0].tag,
                name: match.opponent[0].name,
                score: match.team[0].crowns > match.opponent[0].crowns ? 0 : 1,
                trophy: match.opponent[0].startingTrophies
              },
              date: match.battleTime
            };
            this.addMatch(battle);
          } else {
            const date = new Date(this.parseDate(this.matchs[`${player1}/${player2}`].date)).getTime();
            const battleDate = new Date(this.parseDate(match.battleTime)).getTime();
            if (battleDate < date) {
              this.matchs[`${player1}/${player2}`].player1.score = this.matchs[`${player1}/${player2}`].player1.score + (match.team[0].crowns > match.opponent[0].crowns ? 1 : 0);
              this.matchs[`${player1}/${player2}`].player2.score = this.matchs[`${player1}/${player2}`].player2.score + (match.team[0].crowns > match.opponent[0].crowns ? 0 : 1);
              const battle: Battle = {
                date: match.battleTime,
                player1: this.matchs[`${player1}/${player2}`].player1,
                player2: this.matchs[`${player1}/${player2}`].player2
              }
              this.addMatch(battle);
            }
          }
        }
      });
      setTimeout(() => {
        this.loaderService.hide();
      }, 500);
    });
  }

  parseDate(date: string) {
    return `${date.slice(0, 4)}-${date.slice(4, 6)}-${date.slice(6, 11)}:${date.slice(11, 13)}:${date.slice(13, 20)}` 
  }

  addMatch(battle: Battle) {
    this.matchs[`${battle.player1.id}/${battle.player2.id}`] = battle;
    this._storage.set('matchs', JSON.stringify(this.matchs));
    this.loaderService.hide();
    this.getBattle();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Id incorrect',
      message: 'Le joueur est introuvable, essayez un autre identifiant !',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  removeMatch(match: string) {
    console.log(match);
    delete this.matchs[match];
    this._storage.set('matchs', JSON.stringify(this.matchs));
    this.loaderService.hide();
    this.getBattle();
  }

}
