import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  private _storage: Storage | null = null;

  constructor(private storage: Storage) {}

  async ngOnInit() {
    this._storage = await this.storage.create();
  }

}
