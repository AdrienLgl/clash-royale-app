import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PlayerService } from '../_services/player.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.page.html',
  styleUrls: ['./player.page.scss'],
})
export class PlayerPage {

  private _storage: Storage | null = null;

  constructor(private storage: Storage, private router: NavController, public alertController: AlertController, public playerService: PlayerService) { }

  async ionViewWillEnter() {
    this._storage = await this.storage.create();
  }


  setPlayerId(id: string | number) {
    id = id.toString().replace('#', '');
    this.playerService.findPlayer(id).then((response) => {
      if (response) {
        this._storage.set('player', id);
        this.redirectTo('/tabs/tab1');
      } else {
        this.presentAlert();
      }
    }, (error) => {
      this.presentAlert();
    });
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Id incorrect',
      message: 'Le joueur est introuvable, essayez un autre identifiant !',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  redirectTo(uri:string){
    this.router.navigateBack(['/tabs/tab1/refresh']);
 }

}
