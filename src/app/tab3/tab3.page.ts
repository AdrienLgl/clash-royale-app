/* eslint-disable @typescript-eslint/dot-notation */
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LoaderService } from '../_services/loader.service';
import { PlayerService } from '../_services/player.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  id;
  battlelog: any;
  private _storage: Storage | null = null;

  constructor(private playerService: PlayerService, private loaderService: LoaderService, private storage: Storage) { }

  async ionViewWillEnter() {
    this._storage = await this.storage.create();
    const playerId = await this._storage.get('player');
    this.id = playerId !== undefined || playerId !== null ? playerId : null;
    this.getBattlelog();
  }

  getBattlelog(): void {
    this.loaderService.show();
    this.playerService.getBattlelog(this.id).then((response) => {
      Object.values(response).forEach((battle: any) => {
        if (battle.opponent[0].clan) {
          this.playerService.findBadge(battle.opponent[0].clan.badgeId).then((badge: any) => {
            battle.opponent[0].clan['badge'] = badge;
          });
        } else {
          battle.opponent[0]['clan'] = {
            name: 'Pas de clan', badge: { name: 'no_clan' }
          };
        }

        if (battle.team[0].clan) {
          this.playerService.findBadge(battle.team[0].clan.badgeId).then((badge: any) => {
            battle.team[0].clan['badge'] = badge;
          });
        } else {
          battle.team[0]['clan'] = { name: 'Pas de clan', badge: { name: 'no_clan' } };
        }
      });
      this.battlelog = response;
      setTimeout(() => {
        this.loaderService.hide();
      }, 5000);
    });
  }


}
